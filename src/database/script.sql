-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Teodor
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2022-02-16 10:47
-- Created:       2022-02-14 11:37
PRAGMA foreign_keys = OFF;

-- Schema: mydb

BEGIN;
CREATE TABLE "Role"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "roleName" VARCHAR(45) NOT NULL
);
CREATE TABLE "User"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "username" VARCHAR(45) NOT NULL,
  "password" VARCHAR(45) NOT NULL,
  "role_id" INTEGER NOT NULL,
  "profil" VARCHAR(45) NOT NULL,
  CONSTRAINT "fk_user_role1"
    FOREIGN KEY("role_id")
    REFERENCES "Role"("id")
);
CREATE INDEX "User.fk_user_role1_idx" ON "User" ("role_id");
CREATE TABLE "Humeur"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "humeur" INTEGER NOT NULL,
  "date" DATE NOT NULL,
  "user_id" INTEGER NOT NULL,
  CONSTRAINT "fk_humeur_user"
    FOREIGN KEY("user_id")
    REFERENCES "User"("id")
);
CREATE INDEX "Humeur.fk_humeur_user_idx" ON "Humeur" ("user_id");
COMMIT;
