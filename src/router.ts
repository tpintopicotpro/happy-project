import { Application } from "express";
import UserController from "./controllers/UserController";
import HumeurController from "./controllers/HumeurController";
import HomeController from "./controllers/HomeController";
import AdminController from "./controllers/AdminController";
import { tokenToString } from "typescript";


export default function route(app: Application)
{
    /** Static pages **/
    app.get('/', (req, res) =>
    {

        HomeController.index(req, res);
        
    });

    app.get('/login', (req, res) =>{
        UserController.LoginForm(req, res);
    })

    app.post('/login', (req, res) =>{
        UserController.login(req, res);
    })
    
    app.get('/register', (req,res) => {
        UserController.RegisterForm(req, res);
    })

    app.post('/register',(req, res)=> {
        UserController.register(req, res);
    })

    app.get('/profil/:id', (req, res)=>{
        HumeurController.showProfile(req, res);
    })

    app.post('/profil/:id', (req, res)=>{
        HumeurController.updateHumeur(req, res);
        //HumeurController.mood(req, res)
    })

    app.get('/admin',(req, res)=>{
        AdminController.admin(req, res)
    })

    app.get('/admin-update/:id', (req, res)=>{
        AdminController.showFormUpdate(req, res)
    })
    app.post('/admin-update/:id', (req, res)=>{
        AdminController.update(req, res)
    })

    app.get('/admin-delete/:id', (req, res)=>{
        AdminController.deleteUser(req, res)
    })

    app.get('/admin-create',(req, res)=>{
        AdminController.showForm(req, res)
    })

    app.post('/admin-create', (req, res)=>{
        AdminController.formPost(req, res)
    })
    
    app.get('/API/mood/:id', (req, res) => {
        HumeurController.mood(req, res)
    })
}
