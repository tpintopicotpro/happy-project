import { Request, Response } from "express-serve-static-core";

export default class AdminController {
    static admin(req: Request, res: Response){
        const db = req.app.locals.db;
        const userList = db.prepare("SELECT * FROM User").all();
        console.log(userList);
        res.render('pages/admin',{
            user:userList
        })
    }

    static showFormUpdate(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        const user = db.prepare('SELECT * FROM User WHERE id = ?').get(req.params.id);

        res.render('pages/admin-update', {
            title: 'Modifier Utilisateur',
            user: user
        });
    }


    static update(req: Request, res: Response){
        const db = req.app.locals.db;
        console.log(req.body)
        db.prepare('UPDATE User SET username = ?, password = ?,role_id = ? WHERE id = ?').run(req.body.username,req.body.password,req.body.role, req.params.id);

        AdminController.admin(req, res);

    }
    
    static deleteUser(req: Request, res: Response){
        const db = req.app.locals.db;

        db.prepare('DELETE FROM User WHERE id = ?').run(req.params.id);

        res.redirect('/admin');

    }

    static showForm(req: Request, res: Response){
        const db = req.app.locals.db;
        //const user = db.prepare('INSERT INTO user ( "username", "password", "role_id", "profil") VALUES ( ?, ?, ?, ?)').run(req.body.username, req.body.password, 2, req.body.user);
        res.render('pages/admin-create')
    }

    static formPost(req: Request, res: Response){
        const db = req.app.locals.db;
        const user = db.prepare('INSERT INTO user ( "username", "password", "role_id", "profil") VALUES ( ?, ?, ?, ?)').run(req.body.username, req.body.password, 2, "private"); 
        res.redirect('/admin')
    }
}