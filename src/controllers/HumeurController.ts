// import express
import { Request, Response } from "express-serve-static-core";

export default class HumeurController {

    /**
     * affiche les critères
     * @param req 
     * @param res 
     */
    static criteres(req: Request, res: Response): void {
        // Récupère la DB 
        const db = req.app.locals.db;
        //Récupère le params.id
        const id = req.params.id;
        
        res.render('pages/profil', {
            title: 'Profil',
            
        });
    };

    /**
     * Recupère et traite les données du formulaire
     * @param req 
     * @param res 
     */

    static showProfile(req: Request, res: Response): void {

        // Récupère la DB 
        const db = req.app.locals.db;
        //Récupère le params.id
        const id = req.params.id;
        //creation Array key/value
        let humeur = {"Excellente": 4 , "Bonne": 3 , "Bof": 2 , "Mauvaise":1}
        let a = new Date () 
        const day = a.getDate()
        const month = a.getMonth()
        const year = a.getFullYear()
        // Requete a la DB pour get le Usernameres.render('pages/login'res.render('pages/login');.render('pages/login'););
        const user = db.prepare('SELECT * FROM user WHERE id= ?').all(id)
        console.log(req.params)
        console.log(req.body)
        // db.prepare('INSERT INTO humeur_has_user VALUES(?,?)').run(id,)
        // console.log("mon____user:",user[0].username)
        res.render('pages/profil', {
            title: 'Profil',
            id : id,
            user : user,
            humeur : humeur,
            day : day ,
            month : month,
            year : year,
        });
    };

    static updateHumeur (req: Request, res: Response): void 
    {
        const id = req.params.id;
        const db = req.app.locals.db;
        const date = new Date().toLocaleDateString()
        console.log("mon humeur:",req.body.humeur)
        db.prepare('INSERT INTO Humeur ("humeur", "date", "user_id") VALUES(?,?,?)').run(req.body.humeur, date, id)
    }

    static getAllHumeur (req: Request, res: Response): void{
        const db = req.app.locals.db;
        const id = req.params.id;
        const humeur = db.prepare('SELECT * FROM Humeur WHERE user.id= ?').get(id)

        console.log(humeur)
    }
    static user(req: Request, res: Response): void {
        const db = req.app.locals.db;
        const user = db.prepare('SELECT * FROM user').all()
        const user_json = JSON.stringify(user)
        res.status(200).json(user_json)
    }
    static mood(req: Request, res: Response): void 
    {
        const db = req.app.locals.db;
        const id = req.params.id;
        console.log("mon id:",id)
        const humeur = db.prepare('SELECT humeur , date FROM Humeur WHERE user_id = ? ').all(id)
        console.log("mon humeur:",humeur)
        const user_humeur = JSON.stringify(humeur)
        //console.log("mon user_humeur",user_humeur)
        res.json(user_humeur)
        
    }
}
// Chaque utilisateur enregistré pourra consulter sa tendance d'humeur sur 3j, 7j, 15j (sans devoir recharger la page) representée graphiquement.


